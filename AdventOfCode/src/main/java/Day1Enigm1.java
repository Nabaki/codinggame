import java.util.Scanner;

public class Day1Enigm1 extends AdventOfCode {

    public static void main(String[] args) {
        new Day1Enigm1().process();
    }

    @Override
    public String processInput(Scanner input) {
        int res = 0;
        while (input.hasNext()) {
            int value = input.nextInt();
            System.err.println(value);
            res += Math.floor(value / 3.0) - 2;
        }
        return String.valueOf(res);
    }
}