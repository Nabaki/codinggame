import java.util.Scanner;

public class Day1Enigm2 extends AdventOfCode {

    public static void main(String[] args) {
        new Day1Enigm2().process();
    }

    @Override
    public String processInput(Scanner input) {
        int res = 0;
        while (input.hasNext()) {
            int masse = input.nextInt();
            System.err.println(masse);
            res += calculateFuel(masse);
        }
        return String.valueOf(res);
    }

    public int calculateFuel(int masse){
        return calculateFuelRecursive(masse / 3 - 2);
    }

    private int calculateFuelRecursive(int fuel) {
        int newFuel = fuel / 3 - 2;
        if (newFuel > 0) {
            return fuel + calculateFuelRecursive(newFuel);
        } else {
            return fuel;
        }
    }
}
