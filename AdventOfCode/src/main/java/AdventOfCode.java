import com.google.common.base.CaseFormat;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

abstract class AdventOfCode {

    public void process() {
        System.out.println(this.processFile(getFile()));
    }

    private String processFile(String fileName) {
        String result;
        try (InputStream resourceAsStream = AdventOfCode.class.getClassLoader().getResourceAsStream(fileName)) {
            Scanner sc = null;
            try {
                sc = new Scanner(resourceAsStream);
                result = processInput(sc);
            } finally {
                if (sc != null) {
                    sc.close();
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException("Le fichier " + fileName + " n'a pas été trouvé à la racine du dossier resources.");
        }
        return result;
    }

    protected abstract String processInput(Scanner input);

    private String getFile() {
        return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, this.getClass().getSimpleName()) + ".txt";
    }
}

