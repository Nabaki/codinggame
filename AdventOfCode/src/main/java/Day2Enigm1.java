import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Day2Enigm1 extends AdventOfCode {

    public static void main(String[] args) {
        new Day2Enigm1().process();
    }

    @Override
    public String processInput(Scanner input) {
        int res = 0;
        String line = input.next();
        String[] splittedLine = line.split(",");
        List<Integer> integers = Arrays.stream(splittedLine)
                .map(Integer::valueOf)
                .collect(Collectors.toList());

        integers.set(1, 12);
        integers.set(2, 2);
        processAll(integers);
        return integers.toString();
    }

    public void processAll(List<Integer> integers){
        int idx = 0;
        int opCode = integers.get(idx);
        while (opCode != 99) {
            processOpCode(idx, integers);
            idx += 4;
            opCode = integers.get(idx);
        }
    }

    public void processOpCode(int idx, List<Integer> integers) {
        Integer opCode = integers.get(idx);
        if (opCode.equals(1)) {
            int idx1 = integers.get(idx + 1);
            int idx2 = integers.get(idx + 2);
            int idx3 = integers.get(idx + 3);
            int newValue = integers.get(idx1) + integers.get(idx2);
            integers.set(idx3, newValue);
        } else if (opCode.equals(2)) {
            int idx1 = integers.get(idx + 1);
            int idx2 = integers.get(idx + 2);
            int idx3 = integers.get(idx + 3);
            int newValue = integers.get(idx1) * integers.get(idx2);
            integers.set(idx3, newValue);
        } else {
            throw new IllegalStateException(opCode + " shouldn't appear here.");
        }
    }
}
