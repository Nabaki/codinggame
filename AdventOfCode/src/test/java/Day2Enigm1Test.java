import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class Day2Enigm1Test {

    @Test
    public void processOpCode_0() {
        //GIVEN
        ArrayList<Integer> integers = new ArrayList<>(Arrays.asList(1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50));
        ArrayList<Integer> result = new ArrayList<>(Arrays.asList(1, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50));

        //WHEN
        new Day2Enigm1().processOpCode(0, integers);

        //THEN
        assertThat(integers).isEqualTo(result);
    }

    @Test
    public void processOpCode_4() {
        //GIVEN
        ArrayList<Integer> integers = new ArrayList<>(Arrays.asList(1, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50));
        ArrayList<Integer> result = new ArrayList<>(Arrays.asList(3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50));

        //WHEN
        new Day2Enigm1().processOpCode(4, integers);

        //THEN
        assertThat(integers).isEqualTo(result);
    }

    @Test
    public void processAll() {
        //GIVEN
        ArrayList<Integer> integers = new ArrayList<>(Arrays.asList(1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50));
        ArrayList<Integer> result = new ArrayList<>(Arrays.asList(3500,9,10,70, 2,3,11,0, 99, 30,40,50));

        //WHEN
        new Day2Enigm1().processAll(integers);

        //THEN
        assertThat(integers).isEqualTo(result);
    }
}
