import org.junit.Test;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class Day1Enigm2Test {

    @Test
    public void caculateFuel_1969(){
        assertThat(new Day1Enigm2().calculateFuel(1969)).isEqualTo(966);
    }

    @Test
    public void caculateFuel_100756(){
        assertThat(new Day1Enigm2().calculateFuel(100756)).isEqualTo(50346);
    }
}
