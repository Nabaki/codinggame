package com.isograd.exercise;

import java.util.*;

public class IsoContest {
    public static void main(String[] argv) throws Exception {
        Scanner sc = new Scanner(System.in);
        String line;

        int myIdPilote = -1;
        int nbEvents = -1;
        List<Event> eventList = new ArrayList<>();

        while (sc.hasNext()) {
            line = sc.nextLine();
            System.err.println(line);

            if (myIdPilote == -1) {
                myIdPilote = Integer.valueOf(line);
            } else if (nbEvents == -1) {
                nbEvents = Integer.valueOf(line);
            } else {
                eventList.add(new Event(line));
            }
        }

        List<Integer> positions = new ArrayList<>(20);
        for (int i = 1; i <= 20; i++) {
            positions.add(i);
        }

        eventList.forEach(event -> {
            System.err.println(event);

            switch (event.event) {
                case D:
                    int initialPositionD = positions.indexOf(event.idPilote);
                    int piloteLent = positions.get(initialPositionD -1);

                    positions.set(initialPositionD - 1, event.idPilote);
                    positions.set(initialPositionD, piloteLent);
                    break;
                case I:
                    int initialPositionI = positions.indexOf(event.idPilote);
                    positions.remove(initialPositionI);
                    break;
            }

            System.err.println(positions);
        });


        int piloteIdx = positions.indexOf(myIdPilote);
        if (piloteIdx == -1) {
            System.out.println("KO");
        } else {
            System.out.println(piloteIdx + 1);
        }
    }
}

class Event {
    public final int idPilote;
    public final EventEnum event;

    public Event(String line) {
        String[] s = line.split(" ");
        this.idPilote = Integer.valueOf(s[0]);
        this.event = EventEnum.valueOf(s[1]);
    }

    @Override
    public String toString() {
        return "Event{" +
                "idPilote=" + idPilote +
                ", event=" + event +
                '}';
    }
}

enum EventEnum {
    D,
    I
}