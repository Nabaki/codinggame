import com.isograd.exercise.IsoContest;

import java.io.IOException;
import java.io.InputStream;

public class BattleDev {

    public static void main(String[] args) {

        String inputFileName = "input.txt";
        Runnable runnable = () -> {
            try {
                IsoContest.main(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        };

        InputStream input = System.in;
        try (InputStream resourceAsStream = BattleDev.class.getClassLoader().getResourceAsStream(inputFileName)) {
            System.setIn(resourceAsStream);

            runnable.run();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.setIn(input);
        }
    }
}

