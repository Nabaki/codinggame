object PlayerTest extends App {

  //Creatures sans dégat
  BaseCards.map.toSeq
    .filter(idNCard => idNCard._2.cardType == CardType.CREATURE)
    .groupBy(_._2.cost).toSeq
    .sortBy(_._1)
    .foreach { costNidNCard =>
      val points = costNidNCard._2.map(_._2.points)
      println(s"COST ${costNidNCard._1} -> ${points.sum / points.length}")
    }

  println("------------------------------")

  //Classement des sorts les plus appréciés
  BaseCards.map.toSeq
    .filter(idNCard => idNCard._2.cardType == CardType.CREATURE && idNCard._2.cost == 1)
    //.filter(idNCard => idNCard._2.cardType == CardType.RED || idNCard._2.cardType == CardType.BLUE)
    //.filter(idNCard => idNCard._2.cardType == CardType.GREEN)
    //.filter(idNCard => idNCard._2.points >= 0 && idNCard._2.points < 50)
    .sortBy(idNCard => (idNCard._2.points, idNCard._2.cardNumber))
    .foreach(idNCard => println(s"${idNCard._2.points} -> ${idNCard._2}"))

  println("------------------------------")

  val by = BaseCards.map.toSeq
      .filter(idNCard => idNCard._2.cardType == CardType.CREATURE && idNCard._2.cost != 0)
      .map(idNCard => (idNCard._2.damage + idNCard._2.health) * 20 / idNCard._2.cost)
    println(by.sum / by.length)
}